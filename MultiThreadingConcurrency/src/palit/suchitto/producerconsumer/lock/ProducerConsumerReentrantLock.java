package palit.suchitto.producerconsumer.lock;

import palit.suchitto.producerconsumer.monitor.Buffer;

/**
 * 
 * PROBLEM STATEMENT: There are two processes # A Producer # And a Consumer.
 * 
 * They share a common buffer with a limited capacity. Producer produces data
 * and stores in the buffer. Consumer consumes data and removes it from the
 * buffer. The two processes run in parallel.
 * 
 * We need to make sure that the, # producer will not put data in the buffer
 * when the buffer is full, # consumer will not remove data from buffer when the
 * buffer is empty.
 * 
 * SOLUTION: The producer and the consumer will communicate with each other.
 * 
 * If the buffer is full the producer will wait to be notified. When the
 * consumer removes data from the buffer, the consumer will notify the
 * producer.After notification, the producer will start refilling the buffer
 * again.
 * 
 * If the buffer is empty the consumer will wait to be notified. When the
 * producer adds data in the buffer it will notify the consumer. After
 * notification, the consumer will start to data from the buffer.
 * 
 * 
 * 
 * @author Suchitto
 *
 */
public class ProducerConsumerReentrantLock {

	public static void main(String[] args) throws InterruptedException {
		Buffer buffer = new Buffer(2);

		Runnable producerTask = () -> {
			// PID is necessary to create a thread dump.
			// Works only with JDK 9+
			System.out.println("PID: " + ProcessHandle.current().pid());
			try {
				int value = 0;
				while (value <= 10) {
					buffer.add(value);
					value++;
					Thread.sleep(1000);
				}
			} catch (InterruptedException ie) {
				ie.printStackTrace();
				// Preserve interrupt status. For explanation see
				// "Java Concurrency in Practice" by Brian Goetz et al.
				// Chapter 7.
				Thread.currentThread().interrupt();
			}
		};

		Runnable consumerTask = () -> {
			try {
				int value = 0;
				while (value < 10) {
					value = buffer.poll();
					Thread.sleep(1000);
				}
			} catch (InterruptedException ie) {
				ie.printStackTrace();
				// Preserve interrupt status. For explanation see
				// "Java Concurrency in Practice" by Brian Goetz et al.
				// Chapter 7.
				Thread.currentThread().interrupt();
			}
		};

		Thread producerThread1 = new Thread(producerTask);
		producerThread1.setName("Producer1");
		// Thread producerThread2 = new Thread(producerTask);
		// producerThread2.setName("Producer2");

		Thread consumerThread1 = new Thread(consumerTask);
		consumerThread1.setName("Consumer1");
		// Thread consumerThread2 = new Thread(consumerTask);
		// consumerThread2.setName("Consumer2");

		producerThread1.start();
		// producerThread2.start();
		consumerThread1.start();
		// consumerThread2.start();
		producerThread1.join();
		// producerThread2.join();
		consumerThread1.join();
		// consumerThread2.join();

	}
}
