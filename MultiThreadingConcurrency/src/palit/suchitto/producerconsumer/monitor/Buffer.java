package palit.suchitto.producerconsumer.monitor;

import java.util.LinkedList;
import java.util.Queue;

public class Buffer {
	private Queue<Integer> list;
	private int limit;

	public Buffer(int limit) {
		this.limit = limit;
		this.list = new LinkedList<>();
	}

	public void add(int value) throws InterruptedException {
		synchronized (list) {
			while (list.size() >= limit) {
				// System.out.println(Thread.currentThread().getName() + " Waiting in
				// Buffer.add");
				list.wait();
			}
			list.add(value);
			System.out.println(Thread.currentThread().getName() + " Produced : " + value);
			list.notifyAll();
		}
	}

	public int poll() throws InterruptedException {
		synchronized (list) {
			while (list.size() == 0) {
				// System.out.println(Thread.currentThread().getName() + " Waiting in
				// Buffer.poll");
				list.wait();
			}
			int value = list.poll();
			System.out.println(Thread.currentThread().getName() + " Consumed: " + value);
			list.notifyAll();
			return value;
		}
	}
}
