package palit.suchitto.problem9Good;

import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 
 * The Solution is O(n). Problem Statement: The maximum subarray sum problem is
 * a task to find the series of contiguous numbers with the maximum sum in any
 * given array of n numbers. For instance, in the following array, the subarray
 * from index 3 to index 6 has the maximum sum 6
 * 
 * 3 1 -8 4 -1 2 1 -5 5
 * 
 * Constraints: 0 < n < 50000 where n = number of elements in the array -50000 <
 * i < 50000 where i is the ith element in the array Input at console: The array
 * of n numbers in a line seperated by space
 * 
 * Output at console: The statement:
 * 
 * "Maximum subarray sum is <sum> between indices <i> and <j>."
 * 
 * Input 1: 3 1 -8 4 -1 2 1 -5 5
 * 
 * Output 1: Maximum subarray sum is 6 between indices 3 and 6
 * 
 * Input 2: -1 5 2 -2 1 3 -4 2 -5 6
 * 
 * Output 2: Maximum subarray sum is 9 between indices 1 and 5
 * 
 * @author Suchitto
 *
 */
public class MaximumSubarraySumKadaneAlgorithm {

	private static void maxSubArraySum(int[] arr) {

		int size = arr.length;
		int start = 0;
		int end = 0;

		int maximumSubArraySumSoFar = 0, maxEndingHere = 0;

		for (int i = 0; i < size; i++) {
			if (arr[i] > maxEndingHere + arr[i]) {
				start = i;
				maxEndingHere = arr[i];
			} else {
				maxEndingHere = maxEndingHere + arr[i];
			}

			if (maximumSubArraySumSoFar < maxEndingHere) {
				maximumSubArraySumSoFar = maxEndingHere;
				end = i;
			}
		}
		System.out.println(
				"Maximum subarray sum is " + maximumSubArraySumSoFar + " between indices " + start + " and " + end);
	}

	public static void main(String[] args) {

		try (Scanner in = new Scanner(System.in);) {
			maxSubArraySum(Stream.of(in.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray());
			Stream.of(in.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
		}
	}	
}
