package palit.suchitto.problem2;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/*
 * Problem statement is in the pdf file in the same package.
 * Time limit for all testcases is 50 milliseconds.
 * 
 * @author Suchitto Palit
 */
public class StrangeCounterWithTimeComplexity {

	private static long count(long t) {
		long quotient = t / 3;

		long timeElapsed = 0;
		// At start of first cycle, the result is 3
		long initialCountAtStartOfEachCycle = 3;

		for (long i = 1; i <= quotient; i++) {

			quotient = quotient - (initialCountAtStartOfEachCycle / 3);

			if (timeElapsed + initialCountAtStartOfEachCycle > t) {
				break;
			}
			timeElapsed = timeElapsed + initialCountAtStartOfEachCycle;

			initialCountAtStartOfEachCycle = initialCountAtStartOfEachCycle * 2;
		}

		if ((t - timeElapsed) == 0) {

			return 1;
		} else {

			return (initialCountAtStartOfEachCycle - (t - timeElapsed) + 1);
		}

	}

	public static void main(String[] args) throws IOException {
		try(Scanner scanner = new Scanner(System.in);){
		long t = scanner.nextLong();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		Instant start = Instant.now();
		long result = count(t);
		System.out.println(String.valueOf(result));
		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toMillis();
		System.out.println("TIME: " + timeElapsed + " MilliSeconds");
	}
	}

}
