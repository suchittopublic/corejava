package palit.suchitto.problem4Good;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

/*
 * Problem statement is in the pdf file in the same package. Time limit for all
 * testcases is 4 seconds. The solution meets the time limit. 
 * 
 * This is a good problem. The Solution is O(n).
 * 
 * @author Suchitto
 *
 */
public class JimAndTheSkyscrappers {
	
	public static void main(String[] arg) {
		
		try(Scanner sc = new Scanner(System.in);) {
			
			int noOfBuildings = sc.nextInt();
			
			Deque<Integer> stack = new ArrayDeque<>();
			
			// The index of the count is the same as the
			// height of the building.
			int[] countOfEqualHeightBuildingsWithNoTallerBuildingInBetweenThem = new int[noOfBuildings];
			long result = 0;
			Instant start = Instant.now();
			
			for (int i = 0; i < noOfBuildings; i++) {
				int currentHeight = sc.nextInt();
				
				while (!stack.isEmpty() && stack.peek() < currentHeight) {
					Integer top = stack.pop();
					countOfEqualHeightBuildingsWithNoTallerBuildingInBetweenThem[top]--;
				}
				
				
				if (!stack.isEmpty() && stack.peek() == currentHeight) {
						result += countOfEqualHeightBuildingsWithNoTallerBuildingInBetweenThem[currentHeight];
				}
				
				countOfEqualHeightBuildingsWithNoTallerBuildingInBetweenThem[currentHeight]++;
				
				stack.push(currentHeight);
			}
			System.out.println(2 * result);
			Instant finish = Instant.now();
			long timeElapsed = Duration.between(start, finish).toMillis();
			System.out.println("TIME: " + timeElapsed + " MilliSeconds");
		} 
	}

}
