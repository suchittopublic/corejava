package palit.suchitto.problem1Good;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/*
 * 
 * The median of a set of integers is the midpoint value of the data set for
 * which an equal number of integers are less than and greater than the value.
 * To find the median, you must first sort your set of integers in
 * non-decreasing order, then:
 * 
 * #If your set contains an odd number of elements, the median is the middle
 * element of the sorted sample. In the sorted set {1,2,3}, 2 is the median.
 * 
 * #If your set contains an even number of elements, the median is the average
 * of the two middle elements of the sorted sample. In the sorted set {1,2,3,4},
 * (2+3)/2 = 2.5 is the median.
 * 
 * Given an input stream of n integers, you must perform the following task for
 * each ith integer:
 * 
 * 1. Add the ith integer to a running list of integers. 2. Find the median of
 * the updated list (i.e., for the first element through the ith element). 3.
 * Print the list's updated median on a new line. The printed value must be a
 * double-precision number scaled to 1 decimal place (i.e.,12.3 format).
 * 
 * Input Format: The first line contains a single integer, n, denoting the
 * number of integers in the data stream. Each line i of the n subsequent lines
 * contains an integer, ai, to be added to your list.
 * 
 * Constraints 1 <= n <= 10^5, 0 <= ai <= 10^5
 * 
 * Output Format After each new integer is added to the list, print the list's
 * updated median on a new line as a single double-precision number scaled to 1
 * decimal place (i.e.,12.3 format).
 * 
 * Sample Input 
 * 6 
 * 12 
 * 4 
 * 5 
 * 3 
 * 8 
 * 7
 * 
 * 
 * Sample Output 
 * 12.0 
 * 8.0 
 * 5.0 
 * 4.5 
 * 5.0 
 * 6.0
 * 
 * Explanation There are n = 6 integers, so we must print the new median on a
 * new line as each integer is added to the list.
 * 
 * Max time to execute the program for each testcase = 4 secs.
 * 
 * @author Suchitto Palit
 */

public class RunningMedianWithoutTimeComplexity {

	// NOTE: The following solution is correct but it
	// doesn't consider time complexity. So, it will fail
	// to execute test case 3 to 5 within 4 secs. Actually
	// testcases 3 to 5 will take more than 10 minutes
	// to execute.

	public static void main(String[] args) throws IOException {
		try(Scanner scanner = new Scanner(System.in);){

		List<Integer> inputs = new ArrayList<>();

		int count = scanner.nextInt();
		
		Instant start = Instant.now();

		for (int i = 0; i < count; i++) {
			inputs.add(scanner.nextInt());
			printRunningMedian(inputs);
		}

		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toMillis();
		System.out.println("\n TIME: " + timeElapsed +" miliseconds");
	}
	}
	
	private static void printRunningMedian(List<Integer> inputs) {
		int highestIndex = inputs.size() - 1;

		Collections.sort(inputs);

		double result;
		if (highestIndex % 2 == 1) {
			int rightIndex = (highestIndex + 1) / 2;
			result = (inputs.get(rightIndex) + inputs.get(rightIndex - 1)) / 2.0;
		} else {
			result = inputs.get(highestIndex / 2);
		}

		System.out.println("\n"+result);

	}

}
