package palit.suchitto.problem1Good;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

/*
 * 
 * The median of a set of integers is the midpoint value of the data set for
 * which an equal number of integers are less than and greater than the value.
 * To find the median, you must first sort your set of integers in
 * non-decreasing order, then:
 * 
 * #If your set contains an odd number of elements, the median is the middle
 * element of the sorted sample. In the sorted set {1,2,3}, 2 is the median.
 * 
 * #If your set contains an even number of elements, the median is the average
 * of the two middle elements of the sorted sample. In the sorted set {1,2,3,4},
 * (2+3)/2 = 2.5 is the median.
 * 
 * Given an input stream of n integers, you must perform the following task for
 * each ith integer:
 * 
 * 1. Add the ith integer to a running list of integers. 2. Find the median of
 * the updated list (i.e., for the first element through the ith element). 3.
 * Print the list's updated median on a new line. The printed value must be a
 * double-precision number scaled to 1 decimal place (i.e.,12.3 format).
 * 
 * Input Format: The first line contains a single integer, n, denoting the
 * number of integers in the data stream. Each line i of the n subsequent lines
 * contains an integer, ai, to be added to your list.
 * 
 * Constraints 1 <= n <= 10^5, 0 <= ai <= 10^5
 * 
 * Output Format After each new integer is added to the list, print the list's
 * updated median on a new line as a single double-precision number scaled to 1
 * decimal place (i.e.,12.3 format).
 * 
 * Sample Input 6 12 4 5 3 8 7
 * 
 * 
 * Sample Output 12.0 8.0 5.0 4.5 5.0 6.0
 * 
 * Explanation There are n = 6 integers, so we must print the new median on a
 * new line as each integer is added to the list.
 * 
 * Max time to execute the program for each testcase = 4 secs.
 * 
 * @author Suchitto Palit
 */

public class RunningMedianWithTimeComplexity {

	// NOTE: In the following solution time complexity
	// is addressed using Binary Heap (Max Heap, Mean Heap. Essentially
	// a Binary Tree data structure). So all the 6 test cases will through
	// in less than 4 seconds.

	public static void main(String[] args) {

		try(Scanner in = new Scanner(System.in);){

		int inputLength = in.nextInt();
		PriorityQueue<Integer> minHeap = new PriorityQueue<>(inputLength / 2 + 1);
		PriorityQueue<Integer> maxHeap = new PriorityQueue<>(inputLength / 2 + 1, 
											Collections.reverseOrder());
		Instant start = Instant.now();

		while (inputLength > 0) {

			loadToHeaps(in.nextInt(), minHeap, maxHeap);

			balanceTheHeaps(minHeap, maxHeap);
			
			computeMedianAndPrint(minHeap, maxHeap);
			
			inputLength--;
		}

		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toSeconds();
		System.out.println("TIME: " + timeElapsed + " Seconds");
		
		}
	}

	private static void loadToHeaps(int currentInput, PriorityQueue<Integer> minHeap, 
			PriorityQueue<Integer> maxHeap) {
		if (minHeap.size() == 0 || minHeap.peek() <= currentInput) {
			minHeap.offer(currentInput);
		} else {
			maxHeap.offer(currentInput);
		}
	}

	private static void balanceTheHeaps(PriorityQueue<Integer> minHeap, 
			PriorityQueue<Integer> maxHeap) {
		while ((minHeap.size() - maxHeap.size()) > 1) {
			maxHeap.offer(minHeap.poll());
		}
		while ((maxHeap.size() - minHeap.size()) > 0) {
			minHeap.offer(maxHeap.poll());
		}
	}

	private static void computeMedianAndPrint(PriorityQueue<Integer> minHeap, 
			PriorityQueue<Integer> maxHeap) {
		if (minHeap.size() > maxHeap.size()) {

			System.out.println(minHeap.peek() * 1.0);
		} else {
			System.out.println((minHeap.peek() * 1.0 + maxHeap.peek() * 1.0) / 2);
		}
	}

}
