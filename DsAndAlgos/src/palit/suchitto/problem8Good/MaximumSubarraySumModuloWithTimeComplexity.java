package palit.suchitto.problem8Good;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Problem statement is in the pdf file in the same package. Time limit for all
 * testcases is 4 seconds.  
 * The solution is O(n).
 * 
 * 
 * 
 * @author Suchitto Palit
 *
 */
public class MaximumSubarraySumModuloWithTimeComplexity {
	
	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in);) {

			int noOfTestCases = in.nextInt();

			Instant start = Instant.now();
			
			while (noOfTestCases-- > 0) {
				int arraySize = in.nextInt();
				long moduloInteger = Long.parseLong(in.next());
				long[] cumulativeModuloArray = new long[arraySize];

				// m=7, a = [3,3,9,9,5]
				// cumulativeModuloArray = [3,6,1,3,1]
				for (int i = 0; i < arraySize; i++) {
					long num = Long.parseLong(in.next()) % moduloInteger;
					if (i == 0) {
						cumulativeModuloArray[i] = num;
					} else {
						// this calculates the modulo of the sum up to ith digit
						cumulativeModuloArray[i] 
								= (cumulativeModuloArray[i - 1] + num) % moduloInteger;
					}
				}

				TreeSet<Long> treeSet = new TreeSet<>();
				long max = 0;
				for (long currentSumModulo : cumulativeModuloArray) {
					if (treeSet.isEmpty()) {
						max = currentSumModulo;
						treeSet.add(currentSumModulo);
					} else {
						max = Math.max(max, currentSumModulo);
						Long nextHigher = treeSet.higher(currentSumModulo);
						if (nextHigher != null) {
							max = Math.max(max, moduloInteger 
									- (nextHigher - currentSumModulo));
						}
						treeSet.add(currentSumModulo);
					}
				}
				System.out.println(max);
			}
			Instant finish = Instant.now();
			long timeElapsed = Duration.between(start, finish).toMillis();
			System.out.println("TIME: " + timeElapsed + " MilliSeconds");
		}
	}

}
