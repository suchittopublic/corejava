package palit.suchitto.problem10GoodJava8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 
 * Find out the frequency of words in a file in a descending order of counts and
 * print.
 * 
 * 
 * 
 * @author Suchitto
 *
 */

public class ReverseSortedWordFrequencyInFile {

	public static void main(String[] args) {

		try {

			Files.lines(Paths.get("src/palit/suchitto/problem10GoodJava8/input.txt"))
					.flatMap(line -> Arrays.stream(line.trim().split("[ ,.!?\r\n]")))
					.map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim()).filter(word -> !word.isEmpty())
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet().stream()
					.sorted(Map.Entry.<String, Long> comparingByValue().reversed())
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k, v) -> k, LinkedHashMap::new))
					.forEach((k, v) -> System.out.println(k + " " + v));
			//Alternative 1:
			/*Files.lines(Paths.get("src/palit/suchitto/problem10GoodJava8/input.txt"))
			.flatMap(line -> Stream.of(line.trim().split("[ ,.!?\r\n]")))
			.map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim()).filter(word -> !word.isEmpty())
			.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet().stream()
			.sorted(Map.Entry.<String, Long> comparingByValue().reversed())
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k, v) -> k, LinkedHashMap::new))
			.forEach((k, v) -> System.out.println(k + " " + v));*/
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

	}

}
