package palit.suchitto.problem5;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* SOLVED within time limit O(n)
 * PROBLEM STATEMENT:
 * Zack is suffering from obesity and his doctor has advised him to go for running every day. 
 * However, he only has n days on which he can go for running. The number of calories burned 
 * on the kth day is given by 2 * i + d, where i is the miles he has already covered on the 
 * previous (k-1)th days and d is the number of miles he ran on the kth day.
 * 
 * You are given a collection of not necessarily distinct non-negative integers d1, d2, …, dn. 
 * On any given day where he goes for a run, he runs exactly di kilometers where di belongs to 
 * this collection of numbers. In other words, a number from this collection represents the allowed 
 * number of kilometers he can run on any given day. At the end of n days, he has used up every number 
 * from this collection. That is, all allowed daily values have been used.
 * 
 * Example
 * Suppose the allowed values are 5, 8 and 12. Then he has run 8 km on one day, 12 km on another 
 * and 5 km on another. Suppose the allowed values are 5, 5, 3 and 7. Then in two of the running 
 * days, he has run 5 km, 7 km on another day, and 3 km on another. So given allowed values, and 
 * the number of running days, find out the maximum possible number of calories he can burn at 
 * the end of his n running days.
 * 
 * Constraints
 * 
 * 1<=t<=1000
 * 
 * 1<=n<=10^5
 * 
 * 1<=d<=10^5
 * 
 * Input Format
 * The first line of the input contains t, which gives the number of test cases. Each test 
 * case has 2 lines where, the first line contains an integer n which represents the number 
 * of days. The second line contains n space separated integers that represent the allowed 
 * number of kilomters he can run on an running day.
 * 
 * Output Format
 * The output contains t lines in which the ith line contains an integer C which is the maximum 
 * number of calories he can burn for the ith test case.

Sample Input

11
4
10 20 8 7
5
12 7 4 65 1
10
1 20 4 200 1 100 1093 10 1001 11
9
69 53 54 12 32 87 1 11 82
2
96 97
8
74 52 7 93 41 41 20 24
8
71 18 78 55 48 54 46 16
2
36 98
8
2 76 98 88 97 33 56 31
5
74 77 60 99 38
1
21

Sample Output
221
717
42497
4995
387
3772
3820
330
5035
2018
21


Explanation of Sample Output 221


* Explanation
* For the first test case, maximum calories lost is when he runs 20 km on the first day, 
* 10 on the second day, 8 on the third and 7 on the fourth. This gives 221. The details 
* of this calculation is shown below:
*
* 2 * 0 + 20 = 20
* 2 * 20 + 10 = 50
* 2 * 30 + 8 = 68
* 2 * 38 + 7 = 83
* 
* The total number of calories burned is: 20 + 50 + 68 + 83 = 221 
* 
* Environment & Scoring
* Read from STDIN and write to STDOUT.
* 
* You can check for custom input on our slate https://slate.skillenza.com
* Remove package declarations and keep the class name as “solution” (small case)
* 
* 
* @author Suchitto
*
*/
public class SkillenzaSocieteGeneralProblem1 {
	/*
	 * private static Logger logger =
	 * LoggerFactory.getLogger(SkillenzaSocieteGeneralProblem1.class.getName());
	 */
	public static void main(String[] args) throws Exception {

		try(Scanner in = new Scanner(System.in);){

		int noOfTestCase = Integer.parseInt(in.nextLine());
		int noOfDays;
		List<Integer> kilometers;

		for (int i = 0; i < noOfTestCase; i++) {
			noOfDays = Integer.parseInt(in.nextLine());
			kilometers = convertInputToIntegerList(in.nextLine());
			computeResultsAndPrint(noOfDays, kilometers);
		}
	}
	}

	private static List<Integer> convertInputToIntegerList(String inputString) {
		return Arrays.stream(inputString.split(" ")).map(Integer::parseInt).collect(Collectors.toList());	
	}
	
	private static void computeResultsAndPrint(int days, List<Integer> kilometers) {
		int calories = 0;
		int noOfMilesCoveredTillPreviousDay = 0;

		Collections.sort(kilometers, Collections.reverseOrder()); 
		
		for (Integer i : kilometers ) {
			calories = calories + 2 * noOfMilesCoveredTillPreviousDay + i;
			noOfMilesCoveredTillPreviousDay = noOfMilesCoveredTillPreviousDay + i;
		}
		System.out.println(calories);
		//logger.info(String.valueOf(calories));
	}


}
