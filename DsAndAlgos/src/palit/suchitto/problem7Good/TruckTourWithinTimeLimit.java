package palit.suchitto.problem7Good;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/**
 * Problem statement is in the pdf file in the same package. Time limit for all
 * testcases is 4 seconds. The solution meets the time limit. 
 * 
 * 
 * 
 * 
 * 
 * @author Suchitto
 *
 */
public class TruckTourWithinTimeLimit {
	
	private static int computeStartPosition(int[][] petrolpumps) {

		int noOfPetrolPumps = petrolpumps.length;
		int startPosition = -1;
		int[] petrolPump;
		int totalSurplusKmsFromStartPosition = 0;
		int totalDeficitKmsBeforeStartPosition = 0;

		for (int petrolpumpsRowItr = 0; 
				petrolpumpsRowItr < noOfPetrolPumps; petrolpumpsRowItr++) {

			if (startPosition == -1) {
				startPosition = petrolpumpsRowItr;
			}

			petrolPump = petrolpumps[petrolpumpsRowItr];
			totalSurplusKmsFromStartPosition = totalSurplusKmsFromStartPosition 
												+ petrolPump[0] - petrolPump[1];

			if (totalSurplusKmsFromStartPosition < 0) {
				totalDeficitKmsBeforeStartPosition = totalDeficitKmsBeforeStartPosition
						+ totalSurplusKmsFromStartPosition;
				totalSurplusKmsFromStartPosition = 0;
				startPosition = -1;
			}

			if ((petrolpumpsRowItr == (noOfPetrolPumps - 1))
					&& ((totalSurplusKmsFromStartPosition 
							+ totalDeficitKmsBeforeStartPosition) >= 0)) {

				return startPosition;

			}

		}

		return startPosition;
	}

	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in);) {
			int noOfPetrolPumps = Integer.parseInt(scanner.nextLine().trim());

			int[][] petrolpumps = new int[noOfPetrolPumps][2];

			for (int petrolpumpsRowItr = 0; 
					petrolpumpsRowItr < noOfPetrolPumps; petrolpumpsRowItr++) {
				String[] petrolpumpsRowItems = scanner.nextLine().split(" ");

				for (int petrolpumpsColumnItr = 0; 
						petrolpumpsColumnItr < 2; petrolpumpsColumnItr++) {
					
					int petrolpumpsItem = 
						Integer.parseInt(petrolpumpsRowItems[petrolpumpsColumnItr].trim());
					petrolpumps[petrolpumpsRowItr][petrolpumpsColumnItr] = petrolpumpsItem;
				}
			}

			Instant start = Instant.now();
			
			int result = computeStartPosition(petrolpumps);

			System.out.println(result);
			
			Instant finish = Instant.now();
			long timeElapsed = Duration.between(start, finish).toMillis();
			System.out.println("TIME: " + timeElapsed + " MilliSeconds");
		}
	}


}
